const { leerDB, guardarDB } = require('../helpers/guardarArchivo');
const Material = require('../models/material');
const Materiales= require('../models/materiales');
const { response } = 'express';


const materiales = new Materiales();

let materialDB = leerDB();
if (materialDB) {
  materiales.cargarMaterialesFromArray(materialDB);
}

const materialGet = (req, res = response) => {

  res.json({
    message: 'GET API cargados',
    materialDB
  })
}


const materialPut = (req, res = response) => {
  const { id } = req.params;

  if (id) {
    materiales.eliminarMaterial(id);
    const { material, precio_unitario, precio_total } = req.body;
    const unMaterial = new Material(material, precio_unitario, precio_total);

    unMaterial.getId(id);
    materiales.crearMaterial(unMaterial);
    guardarDB(materiales.listArray);

    materialDB = leerDB();
  }

  res.json({
    message: 'PUT API cargados',
    materialDB
  })
}


const materialPost = (req, res = response) => {

  const { material, precio_unitario, precio_total } = req.body;
  const unMaterial = new Material(material, precio_unitario, precio_total);

  materiales.crearMaterial(unMaterial);
  guardarDB(materiales.listArray);

  const listado = leerDB();
  materiales.cargarMaterialesFromArray();

  res.json({
    message: 'POST API cargados',
    listado
  })
}


const materialDelete = (req, res = response) => {

  const { id } = req.params

  if (id) {
    materiales.eliminarMaterial(id);
    guardarDB(materiales.listArray)
  }

  res.json({
    message: 'carrito de compras eliminado',
  })
}


module.exports = {
  materialGet,
  materialPut,
  materialPost,
  materialDelete
}