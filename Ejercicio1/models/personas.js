
class Personas {

  constructor() {
    this._listado = [];
  }

  crearPersona(persona = {}) {
    this._listado.push(persona);
    return this._listado;
  }

  get listadoArr(){
    const listado = [];
    //me devuelve un arreglo de todas las keys
    Object.keys(this._listado).forEach(key =>{
      const persona = this._listado[key];
      listado.push(persona)
    });
    return listado;
  }


  cargarPersonasFromArray(personas = {}) { 
   personas.forEach(persona => {
    this._listado[persona.id] = persona;
   });
   }
    
    
}
module.exports = Personas;